import './App.css';
import NewTodoForm from "./todos/NewTodos";
import TodoList from "./todos/TodoList";
import {SC_App} from "./todos/StyledComponents";

const App = () => {
  return (
    <SC_App>
      <div className="app-container">
        <h1>My TODOs:</h1>
        <NewTodoForm />
        <TodoList />
      </div>
    </SC_App>
  );
}

export default App;

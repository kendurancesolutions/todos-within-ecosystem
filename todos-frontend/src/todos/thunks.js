import {action_loadTodosInProgress, action_loadTodosSuccess, action_loadTodosFailure,
	action_createTodo, action_toggleComplete, action_removeTodo} from "./actions";

export const thunk_loadTodos = () => async (dispatch, getState) => {
	// dispatched = used to dispatch other redux actions
	// getState() = current state of redux store

	try{
		dispatch(action_loadTodosInProgress())
		const url = 'http://localhost:8080/todos'//-delay'
		const response = await fetch(url, {method:'GET'})
		const todos = await response.json()
		dispatch(action_loadTodosSuccess(todos))
	}catch(e){
		action_loadTodosFailure()
	}
}

export const thunk_addTodo = text => async dispatch => {
	try{
		const url = 'http://localhost:8080/todos'
		const response = await fetch(url, {
			method:   'POST',
			headers:  { 'Content-Type':'application/json' },
			body:     JSON.stringify({ text }),
		})
		const newTodo = await response.json()
		dispatch(action_createTodo(newTodo))
	}catch(e){
		console.log({e})
	}
}
export const thunk_toggleComplete = id => async dispatch => {
	try{
		const url = `http://localhost:8080/todos/${id}/completed`
		const response = await fetch(url, {
			method:   'POST',
			// headers:  { 'Content-Type':'application/json' },
			// body:     JSON.stringify({ id }),
		})
		const updatedTodo = await response.json()
		// console.log({updatedTodo})
		dispatch(action_toggleComplete(updatedTodo))
	}catch(e){
		console.log({e})
	}
}
export const thunk_removeTodo = id => async dispatch => {
	try{
		const url = `http://localhost:8080/todos/${id}`
		const response = await fetch(url, {
			method:   'DELETE',
			// headers:  { 'Content-Type':'application/json' },
			// body:     JSON.stringify({ text }),
		})
		const deletedTodo = await response.json()
		dispatch(action_removeTodo(deletedTodo.id))
	}catch(e){
		console.log({e})
	}
}
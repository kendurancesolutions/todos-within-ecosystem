// File for: Redux
// reducer = a function named after the redux store resource it's managing
import {CREATE_TODO, REMOVE_TODO, TOGGLE_COMPLETE,
        LOAD_TODOS_IN_PROGRESS, LOAD_TODOS_SUCCESS, LOAD_TODOS_FAILURE,
} from "./actions";

const initialState = {  data: [],   isLoading: false, }

export const reducer_todos = (state = initialState, action ) => {
  const { type, payload } = action


  switch(type){
    case CREATE_TODO: {
      const { todo } = payload
      return {...state, data: state.data.concat( todo )}
    }
    case REMOVE_TODO: {
      const { id: removalID } = payload
      return {...state, data: state.data.filter((todo) =>{
        return todo.id !== removalID
      })}
    }
    case TOGGLE_COMPLETE: {
      const { todo: updatedTodo } = payload
      return {...state, data: state.data.map(todo => {
        if(todo.id === updatedTodo.id) return updatedTodo
        return todo
      })}
    }
    case LOAD_TODOS_SUCCESS:{
      const { todos } = payload
      return {...state, isLoading: false, data: todos, }
    }
    case LOAD_TODOS_IN_PROGRESS:{
      return {...state, isLoading: true,}
    }
    case LOAD_TODOS_FAILURE:{
      return {...state, isLoading: false,}
    }
    default:
      return state
  }


}

// File for: Redux
// Fire create TODO action whenever user types something
// into the New TODO form and presses the button

export const CREATE_TODO = 'CREATE_TODO'
export const action_createTodo = todo => ({
    type:  CREATE_TODO,   payload: { todo },
})

export const REMOVE_TODO = 'REMOVE_TODO'
export const action_removeTodo = id => ({
		type:   REMOVE_TODO,  payload: { id },
})

export const TOGGLE_COMPLETE = 'TOGGLE_COMPLETE'
export const action_toggleComplete = todo => ({
		type:   TOGGLE_COMPLETE,  payload: { todo },
})

// For thunk
export const LOAD_TODOS_IN_PROGRESS = 'LOAD_TODOS_IN_PROGRESS'
export const action_loadTodosInProgress = () => ({
		type:   LOAD_TODOS_IN_PROGRESS,
})
export const LOAD_TODOS_SUCCESS = 'LOAD_TODOS_SUCCESS'
export const action_loadTodosSuccess = todos => ({
		type:   LOAD_TODOS_SUCCESS, payload: { todos }
})
export const LOAD_TODOS_FAILURE = 'LOAD_TODOS_FAILURE'
export const action_loadTodosFailure = () => ({
		type:   LOAD_TODOS_FAILURE,
})
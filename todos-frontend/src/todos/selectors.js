import { createSelector } from "reselect";

/**
    Purpose 1: Abstracts away how data is stored aware in Redux.
    Purpose 2: Provides a way for logic for transforming Redux/store data into component data.
 */

// Lower level selectors
export const selector_getTodos         = state => state.todos.data;
export const selector_getTodosLoading  = state => state.todos.isLoading;

// Higher order selectors -- uses memoization | Without createSelector(), it would recompute every time the app re-renders
export const selector_getCompletedTodos  = createSelector(selector_getTodos, todos => todos.filter(todo =>  todo.isCompleted))
export const selector_getIncompleteTodos = createSelector(selector_getTodos, todos => todos.filter(todo => !todo.isCompleted))
// export const selector_getIncompleteTodosIfNotLoading = createSelector(selector_getTodos, selector_getTodosLoading,
// 	(todos, isLoading) => isLoading ? [] : todos.filter(todo => !todo.isCompleted)
// )

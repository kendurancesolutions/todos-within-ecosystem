import styled from "styled-components";
/****************************************** TO-DO List/Item ******************************************/
export const SC_App = styled.div`
	&{ // this
		padding:			0 20px;
		max-width:		700px;
		margin: 			0 auto;
		background-color:white;
		height:				100vh;
		box-shadow: 	12px 0 8px -4px grey, -12px 0 8px -4px grey;
	}
  .app-container{
	  padding-top:	10px;
  }
`
/* Containers */
const SC_BigRedText = styled.div`
	font-size:      48px;
	color:          #FF0000;
	span            { font-weight:bold; }
`
export const SC_ListWrapper       = styled.div`  /* list-wrapper */
	//max-width: 			700px;
	//margin: 				auto;
`
export const SC_TodoItemContainer = styled.div`  /* todo-item-container */
	background: 		#fff;
	border-radius: 	8px;
	margin-top: 		8px;
	padding:	 			16px;
	position: 			relative;
	box-shadow: 		0 4px 8px grey;
`
export const SC_TextContainer     = styled.div`  /* text-container */
	display:        inline-block;
	padding-right:  3rem;
`
export const SC_ButtonsContainer  = styled.div`   /* buttons-container */
	display:        inline-block;
	position: 			absolute;
	top:						9px;
  right: 					12px;
  bottom: 				12px;
`

/* Buttons */
const SC_Button = styled.button`
	display:	 			inline-block;
	color:          white;

	font-size: 			16px;
 	padding: 				8px;
 	border: 				none;
 	border-radius: 	8px;
 	outline: 				none;
 	cursor: 				pointer;
`

/* Loading Todos */
export const SC_LoadingTodos = styled.div`
	div{ 
		text-align:center;
		padding: 50px 0;
	}
`

export const buttonComplete_getBackgroundColor = (isCompleted) => { // exporting just to test..
	return isCompleted ? "green" : "orange"
}
export const SC_ButtonComplete  = styled(SC_Button)`  /* button-complete, button-remove */
		background-color: ${props => buttonComplete_getBackgroundColor(props.isCompleted)};
    // background-color: #22ee22;
`
export const SC_ButtonRemove   = styled(SC_Button)`  /* button-complete, button-remove */
	background-color: #ee2222;
	margin-left: 			8px;
`

/****************************************** New TO-DO ******************************************/
export const SC_NewTodoForm   = styled.div`
	border-radius: 	8px;
	padding: 				16px 8px;
	text-align: 		center;
	box-shadow: 		0 4px 8px grey;

  display:				flex;
  justify-content: space-between;
`
export const SC_NewTodoInput  = styled.input`
	font-size: 			16px;
	padding: 				8px;
	border: 				none;
	border-bottom: 	2px solid #ddd;
	border-radius: 	8px;
	width: 					70%;
	outline: 				none;
`
export const SC_NewTodoButton = styled.button`
	font-size: 				16px;
	padding: 					8px;
	border: 					none;
	border-radius: 		8px;
	outline: 					none;
	cursor: 					pointer;
	margin-left: 			8px;
	width: 						20%;
	background-color: #22ee22;
	
`
import { expect } from "chai";
import { buttonComplete_getBackgroundColor } from "../StyledComponents";

// You don't test the actual styled components themselves, only the logic that's used throughout.
describe('buttonComplete_getBackgroundColor', () => {
	it('Returns green if completed', () => {
		const isCompleted = true
		const expected = 'green'
		const actual = buttonComplete_getBackgroundColor(isCompleted)
		expect(actual).to.equal(expected)
	})
	it('Returns orange if not completed', () => {
		const isCompleted = false
		const expected = 'orange'
		const actual = buttonComplete_getBackgroundColor(isCompleted)
		expect(actual).to.equal(expected)
	})
})
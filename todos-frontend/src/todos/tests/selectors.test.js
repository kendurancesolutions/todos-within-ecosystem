import { expect } from "chai";
import { selector_getCompletedTodos } from "../selectors";

describe('The getCompletedTodos selector', () => {
	it('Returns only completed todos', () => {
		const fakeTodos = [{text:'Read book', isCompleted: true},{text:'Write book', isCompleted: false},]
		const expected = [{text:'Read book', isCompleted: true}]

		// const actual = selector_getCompletedTodos(fakeTodos) // not great for big states)
		const actual = selector_getCompletedTodos.resultFunc(fakeTodos) // returns only the last one

		expect(actual).to.deep.equal(expected)
	})
})
import { expect } from "chai";
import { reducer_todos } from "../reducers";

describe('The todos reducer', () => {
	it('Adds a new todo when a CREATE_TODO action is received', () => {
		// fake current state to pass as first argument to reducer
		const originalState = { isLoading: false,     data: [] }
		// fake action with payload to pass as second argument
		const fakeTODO      = { text:'Do task',       isCompleted:false, }
		const fakeAction    = { type: 'CREATE_TODO',  payload: { todo: fakeTODO, } }
		// create expected and actual states
		const expected      = { isLoading: false,     data: [ fakeTODO ] }
		const actual        = reducer_todos(originalState, fakeAction)
		// compare results using chai
		expect(actual).to.deep.equal(expected)
	})
})
import { useState } from "react";
import { connect } from 'react-redux'
import { thunk_addTodo } from "./thunks";
import { selector_getTodos } from "./selectors";
import {SC_ListWrapper, SC_NewTodoButton, SC_NewTodoForm, SC_NewTodoInput} from "./StyledComponents";


export const NewTodoForm = ({ todos, onCreatePressed }) => {
	const [inputValue, setInputValue] = useState('')

	return(
		<SC_ListWrapper>
			<h3>New Item:</h3>
			<SC_NewTodoForm>
				<SC_NewTodoInput type="text" value={inputValue}
				                 onChange={e => setInputValue(e.target.value)} placeholder="What to do.."/>
				<SC_NewTodoButton onClick={() => {
					const isDuplicateText = todos.some(todo => todo.text === inputValue)
					if(inputValue && !isDuplicateText) {onCreatePressed(inputValue); setInputValue('')}
				}}>Create Todo</SC_NewTodoButton>
			</SC_NewTodoForm>
		</SC_ListWrapper>
	)
}


// Redux
// returns the pieces of the state that the component needs access to
const mapStateToProps     = state     => ({
	todos: selector_getTodos(state),
})

const mapDispatchToProps  = dispatch  => ({
	onCreatePressed: text => dispatch( thunk_addTodo(text) )
})

export default connect(mapStateToProps, mapDispatchToProps)(NewTodoForm)
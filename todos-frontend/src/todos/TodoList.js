import { useEffect } from "react";
import { connect } from "react-redux";
import { thunk_loadTodos, thunk_removeTodo, thunk_toggleComplete } from "./thunks";
import { selector_getTodos, selector_getTodosLoading, selector_getCompletedTodos, selector_getIncompleteTodos } from "./selectors";
import {
	SC_ButtonComplete, SC_ButtonRemove,
	SC_ButtonsContainer,
	SC_ListWrapper, SC_LoadingTodos,
	SC_TextContainer,
	SC_TodoItemContainer
} from "./StyledComponents";


export const TodoList = ({ completedTodos, incompleteTodos, isLoading, startLoadingTodos, onRemovePressed, onToggleComplete }) => {
	useEffect(() => {
		startLoadingTodos()
	}, [])
	const loadingMessage = <SC_LoadingTodos>
		<div>&nbsp;</div>
		<div>Loading todos..<br/><br/>If items do not load, please ensure the server is running.</div>
	</SC_LoadingTodos>
	const content = (
		<SC_ListWrapper>
			{/*<SC_BigRedText>I'm a <span>styled component</span></SC_BigRedText>*/}
			<ListOfTodos todos={incompleteTodos} title="Current Items"    onRemovePressed={onRemovePressed} onToggleComplete={onToggleComplete}/>
			<ListOfTodos todos={completedTodos}  title="Completed Todos"  onRemovePressed={onRemovePressed} onToggleComplete={onToggleComplete}/>
		</SC_ListWrapper>
	)
	return isLoading ? loadingMessage : content
}

const ListOfTodos = ({ title, todos, onRemovePressed, onToggleComplete }) => {
	return(<>
		<h3>{title}:</h3>
		{todos.map((item, key) => {
			return <TodoListItem key={key} todo={item} onRemovePressed={onRemovePressed} onToggleComplete={onToggleComplete}/>
		})}
	</>)
}

const TodoListItem = ({ todo, onRemovePressed, onToggleComplete }) => {
  return(
		<SC_TodoItemContainer id={`task_${todo.id}`}>
			<SC_TextContainer> {todo.text} </SC_TextContainer>
			<SC_ButtonsContainer>
				<SC_ButtonComplete isCompleted={todo.isCompleted} onClick={() => onToggleComplete(todo.id)}>
					Mark Complete
				</SC_ButtonComplete>
				<SC_ButtonRemove onClick={() => onRemovePressed(todo.id)}>Remove</SC_ButtonRemove>
			</SC_ButtonsContainer>
		</SC_TodoItemContainer>
  )
}



const mapStateToProps     = state     => ({
	todos:            selector_getTodos(state),
	isLoading:        selector_getTodosLoading(state),
	completedTodos:   selector_getCompletedTodos(state),
	incompleteTodos:  selector_getIncompleteTodos(state),
})

const mapDispatchToProps  = dispatch  => ({
	onRemovePressed:        id => dispatch( thunk_removeTodo(id)),
	onToggleComplete:       id => dispatch( thunk_toggleComplete(id)),
	startLoadingTodos:      () => dispatch( thunk_loadTodos()),
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)


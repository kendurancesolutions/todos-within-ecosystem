// File for: Redux
import { createStore, combineReducers, applyMiddleware } from "redux";
import { reducer_todos } from './todos/reducers'; // import reducers

/* For redux-persist */
import { persistReducer} from "redux-persist";
import storage from 'redux-persist/lib/storage'
import autoMergeLevel2 from "redux-persist/es/stateReconciler/autoMergeLevel2";

/* For redux-thunk */
import thunk from 'redux-thunk'
import { composeWithDevTools } from "redux-devtools-extension"

const reducers = { todos: reducer_todos }

const rootReducer = combineReducers( reducers ) // from just redux
const persistConfig = { key:  'root',   storage,    stateReconciler:autoMergeLevel2 } // for redux-persist
const persistedReducer = persistReducer(persistConfig, rootReducer) //for redux-persist

// export const configureStore = () => createStore( rootReducer )
export const configureStore = () => createStore(
	persistedReducer,
	composeWithDevTools(applyMiddleware(thunk)),  // replaces the enhancer below
	// window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(),
)